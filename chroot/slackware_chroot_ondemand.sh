#!/bin/sh
# Slackware 32-bit chroot - This in used to start a chroot shell on demand that
# takes down the chroot once you're done with it.

CHROOT_ROOT=${CHROOT_ROOT:-/chroot32}
CHROOT_USER=${CHROOT_USER:-turtleli}

# Bit unclear what's supposed to be bind mounted and what's not.
mount --bind /dev ${CHROOT_ROOT}/dev
mount --bind /dev/shm ${CHROOT_ROOT}/dev/shm
mount -t proc proc ${CHROOT_ROOT}/proc
mount -t sysfs sysfs ${CHROOT_ROOT}/sys
mount -t devpts devpts ${CHROOT_ROOT}/dev/pts -o gid=5,mode=0620
# libudev needs the data in /run/udev to work correctly, so bind mount /run.
mount --bind /run ${CHROOT_ROOT}/run
# pulseaudio requires /var/run/dbus and /tmp to be bind-mounted
mount --bind /tmp ${CHROOT_ROOT}/tmp
mount --bind /var/run/dbus ${CHROOT_ROOT}/var/run/dbus

# Kernel modules and sources - /usr/src may be unnecessary
mount --bind /lib/modules ${CHROOT_ROOT}/lib/modules
mount --bind /usr/src ${CHROOT_ROOT}/usr/src

# Share user and root directories with host
mount --bind /home ${CHROOT_ROOT}/home
mount --bind /root ${CHROOT_ROOT}/root

# I do like the internet.
cp /etc/resolv.conf ${CHROOT_ROOT}/etc

# X11 - home directory now bind mounted, so this is not necessary anymore
#cp /home/${CHROOT_USER}/.Xauthority ${CHROOT_ROOT}/home/${CHROOT_USER}

# Some DEs require this (can't remember if it's before or within chroot)
#xhost +local:

# This gives the user the shell of their choice
chroot ${CHROOT_ROOT} /bin/su -l ${CHROOT_USER}

# Unmount everything once done - may fail (e.g. processes within chroot still
# running), cleanup manually if it does.
umount ${CHROOT_ROOT}/root
umount ${CHROOT_ROOT}/home
umount ${CHROOT_ROOT}/usr/src
umount ${CHROOT_ROOT}/lib/modules
umount ${CHROOT_ROOT}/var/run/dbus
umount ${CHROOT_ROOT}/tmp
umount ${CHROOT_ROOT}/run
umount ${CHROOT_ROOT}/dev/pts
umount ${CHROOT_ROOT}/sys
umount ${CHROOT_ROOT}/proc
umount ${CHROOT_ROOT}/dev/shm
umount ${CHROOT_ROOT}/dev

