#!/bin/sh
# Slackware 32-bit chroot - Run as root once inside chroot, with any external
# binaries (like NVIDIA) in same folder

ARCH=${ARCH:-i686}
NVIDIA_VERSION=${NVIDIA_VERSION:-346.59}

# Don't know much about ARM unfortunately
if [ "$ARCH" = "x86_64" ]; then
	ALTARCH=x86_64
else
	ALTARCH=x86
fi

# Some select stuff from rc.M that should be relevant:
ldconfig
fc-cache -f
update-mime-database /usr/share/mime
update-gtk-immodules
update-gdk-pixbuf-loaders
setarch $ARCH update-pango-querymodules
glib-compile-schemas /usr/share/glib-2.0/schemas

# /etc/ssl/certs is empty and needs updating
update-ca-certificates -f

# aUpdate packages to latest versions
slackpkg update gpg
slackpkg update
setarch i686 slackpkg upgrade-all

# NVIDIA - Download and install the x86 driver, but don't install the kernel
# modules or kernel modules source. Make sure it's the same version as the x64
# version. Save /usr/lib/libEGL.la beforehand - Some versions of the NVIDIA
# installer will happily wipe that out. Also install the vdpau Slackbuild first
# (not strictly necessary, but it may be removed from future drivers).
if [ -x ./NVIDIA-Linux-$ALTARCH-${NVIDIA_VERSION}.run ]; then
	setarch $ARCH ./NVIDIA-Linux-$ALTARCH-${NVIDIA_VERSION}.run \
		--no-kernel-module-source --no-kernel-module
fi

