#!/bin/sh
# Slackware 32-bit chroot - This script is first and only run once.

CHROOT_ROOT=${CHROOT_ROOT:-/chroot32}

PACKAGE_TREE=${PACKAGE_TREE:-slackware-current/slackware}
VERSION=${VERSION:-slackware-current}
MIRROR=${MIRROR:-http://192.168.1.117/} # '/' Necessary at end.

cd ${PACKAGE_TREE}
# Install packages - Skip e, xfce, k, kde, kdei, t and tcl.
installpkg --root ${CHROOT_ROOT} a/*.t?z ap/*.t?z d/*.t?z l/*.t?z n/*.t?z \
	x/*.t?z xap/*.t?z y/*.t?z
cd -

# Remove kernels and kernel modules - Kernel related directories will be bind
# mounted later if anything within the chroot requires kernel modules to be
# loaded.
ROOT=${CHROOT_ROOT} removepkg kernel-generic kernel-huge kernel-modules

# Copy essential configs - Do I need an fstab?
# Long format to avoid bashisms.
cp -a /etc/group ${CHROOT_ROOT}/etc
cp -a /etc/passwd ${CHROOT_ROOT}/etc
cp -a /etc/shadow ${CHROOT_ROOT}/etc
cp -a /etc/hosts ${CHROOT_ROOT}/etc
cp -a /etc/localtime ${CHROOT_ROOT}/etc
cp -a /etc/localtime-copied-from ${CHROOT_ROOT}/etc
cp -a /etc/HOSTNAME ${CHROOT_ROOT}/etc
cp -a /etc/profile.d/lang.* ${CHROOT_ROOT}/etc/profile.d

# Link chroot system dbus to host system dbus
cp -a /var/lib/dbus/machine-id ${CHROOT_ROOT}/var/lib/dbus

# Add local mirror to slackpkg
echo ${MIRROR}${VERSION}/ >> ${CHROOT_ROOT}/etc/slackpkg/mirrors

# Edit ${CHROOT_ROOT}/etc/profile so that login prompts indicate chroot status.
sed -i -r "s/(PS1=')/\1(chroot)/g" ${CHROOT_ROOT}/etc/profile

