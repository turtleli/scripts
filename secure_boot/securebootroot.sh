#!/bin/sh
# Root part of the script (used after securebootuser.sh)

# Mount efivarfs so efi-writevar can use it
mount -t efivarfs none /sys/firmware/efi/efivars/

# Setup the PK, KEK and db keys
efi-updatevar -f PK.esl.signed PK
efi-updatevar -f KEK.esl.signed KEK
efi-updatevar -f db.esl.signed db

# Append the Microsoft KEK and db keys
efi-updatevar -a -f MSKEK.esl.signed KEK
efi-updatevar -a -f MSdbWPCA.esl.signed db
efi-updatevar -a -f MSdbUDSCA.esl.signed db

# Move kernels and bootloaders
mv /boot/efi/EFI/Slackware/vmlinux-3.14.31-souleater \
	/boot/efi/EFI/Slackware/vmlinux-3.14.31-souleater.bak
mv vmlinuz-3.14.31-souleater.signed \
	/boot/efi/EFI/Slackware/vmlinux-3.14.31-souleater
mv /boot/efi/EFI/Slackware/elilo.efi  \
	/boot/efi/EFI/Slackware/elilo.efi.bak
mv elilo.efi.signed \
	/boot/efi/EFI/Slackware/elilo.efi

# Notes
# Filename conventions
# *.esl - file inserted into an ESL
# *.signed - file signed by a private key
# so x.esl.signed is the file x inserted into an ESL using cert-to-efi-sig-list
# and then signed using sign-efi-sig-list with a private key.
#
# The procedure to append a key to KEK or DB is the same in setup and user mode.
# PK can sign KEK, PK and KEK can sign db (info also in other script)
# efi-updatevar -a -f additionalKEKkey.esl.signed KEK
# efi-updatevar -a -f additionaldbkey.esl.signed db
#
# Changing the PK - The signing method differs in user and setup mode but
# the updating method is the same. See the other script for the signing method.
# efi-updatevar -f newPK.esl.signed PK
#

# To clear the KEK and DB keys I think you have to put the machine into setup
# mode and write a signed empty file to clear the entries.
#efi-updatevar -f empty.signed PK
#efi-updatevar -f empty.signed KEK
#efi-updatevar -f empty.signed db
#
# You can then append as usual to re-add keys.

