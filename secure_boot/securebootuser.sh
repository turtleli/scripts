#!/bin/sh

# UEFI Secure boot.
# Sources that should be read
# [A1] - http://blog.hansenpartnership.com/owning-your-windows-8-uefi-platform/
# [A2] - http://blog.hansenpartnership.com/the-meaning-of-all-the-uefi-keys/
# [A3] - http://blog.hansenpartnership.com/uefi-secure-boot/
# [A4] - relevant man pages
# Using Windows? Parts of this apply too (mainly the public keys)
# [B1] - https://technet.microsoft.com/en-us/library/dn747883.aspx
# Optional sources - Very useful, only 2298 pages.
# [C] - UEFI 2.4 Errata B Specification - http://uefi.org/specifications

# [C] GUID FORMAT is aabbccdd-eeff-gghh-iijj-kkllmmnnoopp - Hex values only
# Choose whatever you want.
OWNER_GUID=${OWNER_GUID:-f00dcafe-beef-c0fe-e995-18ed1b7ef00d}

# Create self signed platform, key exchange and signature database
# public/private keys. UEFI. [A1] They don't have to be self signed, but I don't
# know much about that stuff.
# My convention - .crt = PEM format, .der = DER format

# [A1] The Common Name doesn't matter much.
PK_OWNER=${PK_OWNER:-turtleli Platform Key};
KEK_OWNER=${KEK_OWNER:-turtleli Key Exchange Key};
DB_OWNER=${DB_OWNER:-turtleli Signature Database};

# Create public/private keys in PEM format - if you prefer DER format add
# "-keyform DER -outform DER"
openssl req -new -x509 -newkey rsa:2048 -subj "/CN=${PK_OWNER}/" \
	-keyout PK.key -out PK.crt -days 3650 -nodes -sha256
openssl req -new -x509 -newkey rsa:2048 -subj "/CN=${KEK_OWNER}/" \
	-keyout KEK.key -out KEK.crt -days 3650 -nodes -sha256
openssl req -new -x509 -newkey rsa:2048 -subj "/CN=${DB_OWNER}/" \
	-keyout db.key -out db.crt -days 3650 -nodes -sha256
# [C]28.3.1 - The Platform Key has the format of a signature database with
# exactly one entry.
# Converts x509 certificates (PEM format) into EFI Signature Lists (which is
# essentially a header followed by the certificate converted to DER format -

cert-to-efi-sig-list -g ${OWNER_GUID} PK.crt PK.esl
cert-to-efi-sig-list -g ${OWNER_GUID} KEK.crt KEK.esl
cert-to-efi-sig-list -g ${OWNER_GUID} db.crt db.esl

# [C]28.3.1 - PK can be written if
# Setup mode - PK public key signed with corresponding PK private key
sign-efi-sig-list -k PK.key -c PK.crt PK PK.esl PK.esl.signed
# User mode - PK public key signed with current PK private key
#sign-efi-sig-list -k PK_current.key -c PK_current.crt PK PK.esl PK.esl.signed

# [C]28.3.2 - PK can be cleared by writing 0 bytes signed with current private
# key
#touch empty
#sign-efi-sig-list -k PK_current.key -c PK_current.crt PK empty empty.signed

# [C] 28.3.3 - KEK can be written
# Setup mode - as is.
# User mode - if signed with PK private key in user.
# (If appending, use -a as well)
sign-efi-sig-list -k PK.key -c PK.crt KEK KEK.esl KEK.esl.signed
# [C] 28.5.3 - db can be written
# Setup mode - as is.
# User mode - if signed with either KEK private key or PK private key.
# (If appending, use -a as well)
sign-efi-sig-list -k KEK.key -c KEK.crt db db.esl db.signed

# [B] If you use Windows, the Microsoft KEK and db are needed
# Microsoft Corporation KEK CA 2011
# curl -L http://go.microsoft.com/fwlink/?LinkID=321185 -o MSKEK.der
# Microsoft Windows Production CA 2011
# curl -L http://go.microsoft.com/fwlink/?LinkId=321192 -o MSdbWPCA.der
# Microsoft UEFI Driver Signing CA
# curl -L http://go.microsoft.com/fwlink/?LinkId=321194 -o MSdbUDSCA.der

#Convert to PEM first
openssl x509 -in MSKEK.der -inform DER -out MSKEK.crt
openssl x509 -in MSdbWPCA.der -inform DER -out MSdbWPCA.crt
openssl x509 -in MSdbUDSCA.der -inform DER -out MSdbUDSCA.crt

#Convert to ESL 
MS_GUID=77fa9abd-0359-4d32-bd60-28f4e78f784b
cert-to-efi-sig-list -g ${MS_GUID} MSKEK.crt MSKEK.esl
cert-to-efi-sig-list -g ${MS_GUID} MSdbWPCA.crt MSdbWPCA.esl
cert-to-efi-sig-list -g ${MS_GUID} MSdbUDSCA.crt MSdbUDSCA.esl

#Sign with PK
sign-efi-sig-list -a -k PK.key -c PK.crt KEK MSKEK.esl MSKEK.esl.signed
sign-efi-sig-list -a -k PK.key -c PK.crt db MSdbWPCA.esl MSdbWPCA.esl.signed
sign-efi-sig-list -a -k PK.key -c PK.crt db MSdbUDSCA.esl MSdbUDSCA.esl.signed

# Sign EFI binaries - elilo.efi is actually /tmp/elilo/elilo/elilo.efi after
# running the elilo.SlackBuild. It's not the default elilo.efi.
sbsign --key db.key --cert db.crt --output elilo.efi.signed \
	/boot/efi/EFI/Slackware/elilo.efi
sbsign --key db.key --cert db.crt --output vmlinuz-3.14.31-souleater.signed \
	/boot/efi/EFI/Slackware/vmlinuz-3.14.31-souleater

# Notes
# sbsigntools - DER format version (untested)
#openssl req -new -x509 -newkey rsa:2048 -subj "/CN=${PK_OWNER}/" \
#	-keyout PK.keyder -out PK.der -days 3650 -nodes -sha256 \
#	-keyform DER -outform DER
#openssl req -new -x509 -newkey rsa:2048 -subj "/CN=${KEK_OWNER}/" \
#	-keyout KEK.keyder -out KEK.der -days 3650 -nodes -sha256
#	-keyform DER -outform DER
#openssl req -new -x509 -newkey rsa:2048 -subj "/CN=${DB_OWNER}/" \
#	-keyout db.keyder -out db.der -days 3650 -nodes -sha256
#	-keyform DER -outform DER
#
#sbsiglist --owner ${OWNER_GUID} --type x509 PK.der --output PK.esl
#sbsiglist --owner ${OWNER_GUID} --type x509 KEK.der --output KEK.esl
#sbsiglist --owner ${OWNER_GUID} --type x509 db.der --output db.esl
#
#sbvarsign --key PK.key --cert PK.crt

